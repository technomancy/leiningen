<!DOCTYPE html>
<head lang="en">
    <meta charset="utf-8">
    <title>Leiningen</title>
    <meta name="description" content="Leiningen: automating Clojure projects">
    <meta name="author" content="Phil Hagelberg and contributors">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport"
          content="width=device-width, initial-scale=1">

    <!-- Favicons + Manifest
  ================================================== -->
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
  <link rel="manifest" href="/img/favicon/manifest.json">
  <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#000000">
  <link rel="shortcut icon" href="/img/favicon/favicon.ico">
  <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

    <!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="stylesheets/base.css">
    <link rel="stylesheet" href="stylesheets/skeleton.css">
    <link rel="stylesheet" href="stylesheets/layout.css">
    <link rel="stylesheet" href="stylesheets/htmlize.css">
    <link rel="stylesheet" href="stylesheets/lein.css">
</head>
<body>

  <div class="container">
    <div class="three columns offset-by-one">
      <img src="img/leiningen.jpg" title="logo" id="logo">
    </div>
    <div class="eight columns offset-by-two" id="title">
      <h1 class="remove-bottom" style="margin-top: 40px">Leiningen</h1>
      <p id="tagline">for automating Clojure projects without setting
        your hair on fire</p>
      <nav id="links" class="offset-by-three">
        <a href="#install">install</a> |
        <a href="#docs">docs</a> |
        <a href="#community">community</a> |
        <a href="https://codeberg.org/leiningen/leiningen">source</a>
      </nav>
    </div>

    <div class="sixteen columns"><hr /></div>

    <div class="one-third column">
      <p id="pitch">Leiningen is <strong>the easiest way to use
      <a href="https://clojure.org">Clojure</a></strong>. With a focus
      on project automation and declarative configuration, it gets out
      of your way and lets you focus on your code.</p>
    </div>
    <div class="two-thirds column">
    <pre class="htmlize" id="sample-project"><span class="esk-paren">(</span>defproject leiningen.org <span class="string">"1.0.0"</span>
  <span class="constant">:description</span> <span class="string">"Generate static HTML for https://leiningen.org"</span>
  <span class="constant">:dependencies</span> [[enlive <span class="string">"1.0.1"</span>]
                 [cheshire <span class="string">"4.0.0"</span>]
                 [org.markdownj/markdownj <span class="string">"0.3.0-1.0.2b4"</span>]]
  <span class="constant">:main</span> leiningen.web<span class="esk-paren">)</span></pre>
    </div>

    <div class="sixteen columns"><hr /></div>

    <div class="one-third column"><h3 class="offset-by-one">Install</h3></div>
    <div class="two-thirds column">
      <p>First check
      your <a href="https://wiki.leiningen.org/Packaging">package
      manager</a> to see whether it includes Leiningen. If not, don't
      worry; a manual install is easy.</p>

      <p>Leiningen and Clojure require Java; ideally
        <a href="https://adoptium.net">OpenJDK</a>. Get this from
        your package manager, or download manually if you have to.</p>
      <a name="install"></a>
      <ol>
        <li>Download the <a href="https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein"><kbd>lein</kbd>
        script</a> (or on
          Windows <a href="https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein.bat"><kbd>lein.bat</kbd></a>
          if you don't use WSL)</li>
        <li>Place it on your <tt>$PATH</tt> where your shell can find it (eg. <tt>/usr/local/bin/</tt>)</li>
        <li>Set it to be executable (<kbd>sudo chmod a+x /usr/local/bin/lein</kbd>)</li>
        <li>Run it (<kbd>lein</kbd>) and it will download the self-install package</li>
      </ol>
    </div>

    <div class="sixteen columns"><hr /></div>

    <div class="one-third column" style="float: right">
      <h3 class="offset-by-one">Documentation</h3></div>

    <div class="two-thirds column">
      <a name="docs"></a>
      <p>The <a href="/tutorial.html">tutorial</a>
        is the best place to start. If you have Leiningen installed,
        you can read the tutorial by running <kbd>lein help
        tutorial</kbd>. It does not cover learning the
        language itself;
        good <a href="https://clojure-doc.org">Clojure
        documentation</a> can be found elsewhere.</p>


      <p>Running <kbd>lein help faq</kbd> will get you the
        <a href="/faq.html">FAQ</a>.
        Documentation for each individual task is available
        via <kbd>lein help $TASK</kbd>. You can also
        see <a href="https://codeberg.org/leiningen/leiningen/src/stable/sample.project.clj">the
        sample <tt>project.clj</tt> file</a> containing a reference of most
        project settings by running <kbd>lein help sample</kbd>.</p>

      <p>More:
        <a href="/profiles.html">profiles</a> |
        <a href="/deploy.html">deploying libraries</a> |
        <a href="/plugins.html">writing plugins</a> |
        <a href="https://wiki.leiningen.org/Plugins">plugin list</a>
      </p>
    </div>

    <div class="sixteen columns"><hr /></div>

    <div class="one-third column"><h3 class="offset-by-one">Community</h3></div>
    <div class="two-thirds column">
      <a name="community"></a>
      <p>At the time of this writing Leiningen is the most
        widely-contributed-to Clojure project. We welcome potential
        contributors and do our best to make it easy to help out.</p>

      <p>Discussion occurs primarily <a href="irc://irc.libera.chat#leiningen">in
          the <tt>#leiningen</tt> channel</a>
        on <a href="https://libera.chat">Libera.Chat</a>.</p>

      <p>Issues should
        be <a href="https://codeberg.org/leiningen/leiningen/issues">reported
        on the issue tracker</a>.</p>

      <p>The <a href="https://codeberg.org/leiningen/leiningen/issues">canonical
          repository is on Codeberg</a> but a mirror will be left up
        <a href="https://github.com/technomancy/leiningen">on
        Github</a> for a while for people who have not yet created a
        Codeberg account.</p>
    </div>

    <div class="sixteen columns"><hr /></div>

    <div class="sixteen columns" id="footer">
        <p>&copy; 2012-2024 Phil Hagelberg
        and contributors. Licensed under
        the <a href="https://codeberg.org/leiningen/leiningen/src/stable/COPYING">EPL
        1.0</a>. See the code
        for <a href="https://codeberg.org/leiningen/leiningen/src/main/web">this
        site</a>.</p>
    </div>
    </div><!-- container -->

</body>
</html>
